# frozen_string_literal: true

name 'iptables-services'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/iptables-services@incoming.gitlab.com'
license 'Apache-2.0'
description 'Install/Configure iptables-services on linux nodes'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/iptables-services'
issues_url 'https://gitlab.com/chef-platform/iptables-services/issues'
version '2.4.0'

chef_version '>= 12.14'

supports 'centos', '>= 7.3'

depends 'cluster-search'
